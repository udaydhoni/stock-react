import React from 'react';
import './App.css';

class ImageComponent extends React.Component {
  constructor(props) {
    super ()
  }
  render () {
    return (
      <div className='imageCont'>
        <img src={this.props.imageURL}></img>
      </div>
    )
  }
}

class App extends React.Component {
  constructor (props) {
    super ()
    this.state = {
      search: '',
      images: []

    }
  }
  accessKey = 'vNQPbCvwt0Pq3Xa1puetkxA25IfI7sYYfCh8TyDByvY'
  randomURL = `https://api.unsplash.com/photos/random?client_id=${this.accessKey}&count=10`

  urlFunction = (url=this.randomURL) => {
    if (this.state.search !== '') {
      url = `https://api.unsplash.com/photos/random?client_id=${this.accessKey}&query=${this.state.search}&count=30`
    }
    fetch(url)
    .then((obj)=> obj.json())
    .then((data)=> {
      this.setState({
        images: data
      }) 
    })
  }

  componentDidMount () {
    this.urlFunction()
  }

  searchHandler = (event) => {
    this.setState({
      search: event.target.value
      
    })
  }

  componentDidUpdate (prevProps,prevState) {
    if (prevState.search !== this.state.search) {
      console.log(this.state)
      this.urlFunction()
    }
    
  }


  
  render () {
      return (
        <div>
             <div className='top-bar'>
            <input type='text' placeholder='Search'  onChange = {this.searchHandler} ></input>
            <button onClick={this.searchHandler}>Search</button>
          </div>
          <div className='container'>
          {this.state.images.length === 0 ?  <h1>Loading...</h1> : this.state.images.map((img) => <ImageComponent imageURL = {img.urls.regular}/>)}
          </div>
          
        </div>
        
      )
  }
}

export default App;
